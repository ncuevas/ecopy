from django.contrib import admin
from django.contrib.auth.models import User, Group
from core.models import *
# Register your models here.

admin.site.register(User)
admin.site.register(FootprintWater)
admin.site.register(FootprintEnergy)
admin.site.register(FootprintTransport)
admin.site.register(FootprintWaste)
