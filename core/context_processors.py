from django.conf import settings
import logging

from core import settings as core_settings


def get_globals(request):
    return {'STATIC_URL': settings.STATIC_URL,
            'PROJECT_HOST': settings.PROJECT_HOST,
            'PROJECT_STATIC_PATH': settings.PROJECT_STATIC_PATH,
            }
