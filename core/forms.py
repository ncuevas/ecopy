from core.models import FootprintWater, FootprintEnergy, FootprintTransport, FootprintWaste
from django.forms import ModelForm
from captcha.fields import CaptchaField


class FootprintEnergyForm(ModelForm):
    captcha = CaptchaField()

    class Meta:
        model = FootprintEnergy
        exclude = ['id', 'creation_date', 'last_update']


class FootprintTransportForm(ModelForm):
    captcha = CaptchaField()

    class Meta:
        model = FootprintTransport
        exclude = ['id', 'creation_date', 'last_update']


class FootprintWaterForm(ModelForm):
    captcha = CaptchaField()

    class Meta:
        model = FootprintWater
        exclude = ['id', 'creation_date', 'last_update']


class FootprintWasteForm(ModelForm):
    captcha = CaptchaField()

    class Meta:
        model = FootprintWaste
        exclude = ['id', 'creation_date', 'last_update']
