"""
    Constants File.
"""

__author__ = "Nicolas Cuevas"
__email__ = "nicolas.c@newwaveweb.com"
__version__ = "1.0"
__maintainer__ = "Nicolas Cuevas"
__copyright__ = "Copyright 2015, Medical Web Experts"
__status__ = "Development"


GENDER = (
    	('m', 'Male'),
    	('f', 'Female')
    )