# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _


class FootprintEnergy(models.Model):
    """
    A fully featured User model with admin-compliant permissions that uses
    a full-length email field as the username.

    Email and password are required. Other fields are optional.
    """
    __author__ = "Nicolas Cuevas"
    __email__ = ""
    __version__ = "1.0"
    __maintainer__ = "Nicolas Cuevas"
    __copyright__ = "Copyright 2016"
    __status__ = "Development"

    id = models.AutoField(primary_key=True, db_index=True)
    people_house_quantity = models.IntegerField(null=True, blank=True)
    solid_combustible_waste_kep_person = models.FloatField(default=0)
    solid_combustible_waste_kep_house = models.FloatField(default=0)
    liquid_combustible_waste_kep_person = models.FloatField(default=0)
    liquid_combustible_waste_kep_house = models.FloatField(default=0)
    gases_combustible_waste_kep_person = models.FloatField(default=0)
    gases_combustible_waste_kep_house = models.FloatField(default=0)
    gases_natural_combustible_waste_kep_person = models.FloatField(default=0)
    gases_natural_combustible_waste_kep_house = models.FloatField(default=0)
    electrical_kwh_person = models.FloatField(default=0)
    electrical_kwh_house = models.FloatField(default=0)
    solar_kwh_person = models.FloatField(default=0)
    solar_kwh_house = models.FloatField(default=0)
    total_kep_person = models.FloatField(default=0)
    total_kep_house = models.FloatField(default=0)
    creation_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "core_footprint_energy"
        app_label = "core"
        verbose_name = _('Footprint - Energy')
        verbose_name_plural = _('Footprints - Energy')

    def __unicode__(self):
        return u'{c}'.format(c=self.id)
