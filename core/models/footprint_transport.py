# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _


class FootprintTransport(models.Model):
    """
    A fully featured User model with admin-compliant permissions that uses
    a full-length email field as the username.

    Email and password are required. Other fields are optional.
    """
    __author__ = "Nicolas Cuevas"
    __email__ = ""
    __version__ = "1.0"
    __maintainer__ = "Nicolas Cuevas"
    __copyright__ = "Copyright 2016"
    __status__ = "Development"

    id = models.AutoField(primary_key=True, db_index=True)
    urban_journey_bus_kep = models.FloatField(default=0)
    interurban_journey_bus_kep = models.FloatField(default=0)
    journey_metro_kep = models.FloatField(default=0)
    journey_train_kep = models.FloatField(default=0)
    journey_car_kep = models.FloatField(default=0)
    journey_motorcycle_kep = models.FloatField(default=0)
    journey_bicycle_kep = models.FloatField(default=0)
    journey_foot_kep = models.FloatField(default=0)
    journey_total_kep = models.FloatField(default=0)
    displacements_total_kep = models.FloatField(default=0)
    distance_total_km = models.FloatField(default=0)
    times_around_world = models.FloatField(default=0)
    combustible_waste_kep = models.FloatField(default=0)
    creation_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "core_footprint_transport"
        app_label = "core"
        verbose_name = _('Footprint - Transport')
        verbose_name_plural = _('Footprints - Transport')

    def __unicode__(self):
        return u'{c}'.format(c=self.id)
