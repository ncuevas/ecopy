# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _


class FootprintWaste(models.Model):
    """
    A fully featured User model with admin-compliant permissions that uses
    a full-length email field as the username.

    Email and password are required. Other fields are optional.
    """
    __author__ = "Nicolas Cuevas"
    __email__ = ""
    __version__ = "1.0"
    __maintainer__ = "Nicolas Cuevas"
    __copyright__ = "Copyright 2016"
    __status__ = "Development"

    id = models.AutoField(primary_key=True, db_index=True)
    people_house_quantity = models.IntegerField(null=True, blank=True)
    total_day_waste_kep = models.FloatField(default=0)
    dwa_difference_percentage = models.FloatField(default=0)
    organic_percentage = models.FloatField(default=0)
    paper_cardboard_percentage = models.FloatField(default=0)
    glass_percentage = models.FloatField(default=0)
    plastics_percentage = models.FloatField(default=0)
    metals_percentage = models.FloatField(default=0)
    bricks_percentage = models.FloatField(default=0)
    others_percentage = models.FloatField(default=0)
    creation_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "core_footprint_waste"
        app_label = "core"
        verbose_name = _('Footprint - Waste')
        verbose_name_plural = _('Footprints - Waste')

    def __unicode__(self):
        return u'{c}'.format(c=self.id)
