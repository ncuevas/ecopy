# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _


class FootprintWater(models.Model):
    """
    A fully featured User model with admin-compliant permissions that uses
    a full-length email field as the username.

    Email and password are required. Other fields are optional.
    """
    __author__ = "Nicolas Cuevas"
    __email__ = ""
    __version__ = "1.0"
    __maintainer__ = "Nicolas Cuevas"
    __copyright__ = "Copyright 2016"
    __status__ = "Development"

    id = models.AutoField(primary_key=True, db_index=True)
    people_house_quantity = models.IntegerField(null=True, blank=True)
    total_litre = models.FloatField(default=0)
    dwa_difference = models.FloatField(default=0)
    dwa_percentage = models.FloatField(default=0)
    total_hm = models.FloatField(default=0)
    creation_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "core_footprint_water"
        app_label = "core"
        verbose_name = _('Footprint - Water')
        verbose_name_plural = _('Footprints - Water')

    def __unicode__(self):
        return u'{c}'.format(c=self.id)
