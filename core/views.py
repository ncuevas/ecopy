from django.shortcuts import render
from django.http import HttpResponse
from core.forms import FootprintWaterForm, FootprintEnergyForm, FootprintTransportForm, FootprintWasteForm
from Pysolar import *
import datetime
import json

# Create your views here.
solar1 = solar
radiation1 = radiation


def home(request):
    return render(request, 'index.html', {})


def footprint(request):
    return render(request, 'footprint.html', {})


def energy_footprint(request):
    messages = None
    if request.POST:
        form = FootprintEnergyForm(request.POST)
        if form.is_valid():
            messages = {'showError': False}
            form.save()
        else:
            messages = {'showError': True}
    else:
        form = FootprintEnergyForm()
    return render(request, 'footprint_energy.html', locals())


def transport_footprint(request):
    messages = None
    if request.POST:
        form = FootprintTransportForm(request.POST)
        if form.is_valid():
            messages = {'showError': False}
            form.save()
        else:
            messages = {'showError': True}
    else:
        form = FootprintTransportForm()
    return render(request, 'footprint_transport.html', locals())


def waste_footprint(request):
    messages = None
    if request.POST:
        form = FootprintWasteForm(request.POST)
        if form.is_valid():
            messages = {'showError': False}
            form.save()
        else:
            messages = {'showError': True}
    else:
        form = FootprintWasteForm()
    return render(request, 'footprint_waste.html', locals())


def water_footprint(request):
    messages = None
    if request.POST:
        form = FootprintWaterForm(request.POST)
        if form.is_valid():
            messages = {'showError': False}
            form.save()
        else:
            messages = {'showError': True}
    else:
        form = FootprintWaterForm()
    return render(request, 'footprint_water.html', locals())


def renewable(request):
    return render(request, 'renewable.html', {})


def renewable_solar(request):
    return render(request, 'renewable_solar.html', {})


def renewable_eolic(request):
    return render(request, 'renewable_eolic.html', {})


def renewable_termal(request):
    return render(request, 'renewable_termal.html', {})


def renewable_hidraulic(request):
    return render(request, 'renewable_hidraulic.html', {})


def renewable_bio(request):
    return render(request, 'renewable_bio.html', {})


def radiation(request):
    return render(request, 'radiation.html', {})


def radiation_solar_calc(request):
    #latitude_deg = 42.206
    #longitude_deg = -71.382
    #d = datetime.datetime.utcnow()
    #altitude_deg = solar1.GetAltitude(latitude_deg, longitude_deg, d)
    #alt = (altitude_deg)
    #day_of_year = datetime.datetime.utcnow().timetuple().tm_yday
    #azimuth_deg = solar1.GetAzimuth(latitude_deg, longitude_deg, d)
    #power = radiation1.GetRadiationDirect(day_of_year, alt)
    return render(request, 'radiation_solar_calc.html', {})
