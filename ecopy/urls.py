"""ecopy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.conf.urls import include
from django.contrib import admin

from core import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^$', views.home),
    url(r'^huella$', views.footprint),
    url(r'^huella/agua/$', views.water_footprint),
    url(r'^huella/energia/$', views.energy_footprint),
    url(r'^huella/transporte/$', views.transport_footprint),
    url(r'^huella/residuos/$', views.waste_footprint),
    url(r'^energias-renovables$', views.renewable),
    url(r'^energias-renovables/solar/$', views.renewable_solar),
    url(r'^energias-renovables/eolica/$', views.renewable_eolic),
    url(r'^energias-renovables/hidraulica/$', views.renewable_hidraulic),
    url(r'^energias-renovables/termica/$', views.renewable_termal),
    url(r'^energias-renovables/biomasa/$', views.renewable_bio),
    url(r'^radiacion$', views.radiation),
    url(r'^radiacion/calcular-radiacion-solar$', views.radiation_solar_calc),
]
