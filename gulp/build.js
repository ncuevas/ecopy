'use strict';

var path = require('path'),
gulp = require('gulp'),
conf = require('./conf'),
gp_concat = require('gulp-concat'),
gp_rename = require('gulp-rename'),
gp_uglify = require('gulp-uglify'),
concatCss = require('gulp-concat-css'),
cssmin = require('gulp-cssmin'),
sass = require('gulp-sass'),
$ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});


gulp.task('js', function(){
    return gulp.src([
            path.join(conf.paths.bower, 'angular/angular.js'),
            path.join(conf.paths.bower, 'angular-animate/angular-animate.js'),
            path.join(conf.paths.bower, 'angular-aria/angular-aria.js'),
            path.join(conf.paths.bower, 'angular-material/angular-material.js'),
            path.join(conf.paths.bower, 'angular-messages/angular-messages.js'),
            path.join(conf.paths.bower, 'jquery/dist/jquery.js'),
            path.join('!' + conf.paths.src, 'js/*.{js}')
        ])
        .pipe(gp_concat('concat.js'))
        .pipe(gulp.dest(conf.paths.dist))
        .pipe(gp_rename('uglify.js'))
        .pipe(gp_uglify())
        .pipe(gulp.dest(conf.paths.dist));
});

gulp.task('sass', function () {
  return gulp.src([
            path.join(conf.paths.bower, 'angular-material/angular-material.scss'),
        ])
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(conf.paths.dist));
});


gulp.task('css-concat', function () {
  return gulp.src([
            path.join(conf.paths.bower, 'angular-material/angular-material.css'),
            path.join(conf.paths.bower, 'angular-material/angular-material.layouts.css')
        ])
    .pipe(concatCss("dist.css"))
    .pipe(gulp.dest(conf.paths.dist));
});

gulp.task('cssmin', function () {
    gulp.src([
            path.join(conf.paths.dist, 'dist.css')
        ])
        .pipe(cssmin())
        .pipe(gp_rename('dist.css.min'))
        .pipe(gulp.dest(conf.paths.dist));
});


gulp.task('other', function ()
{
    var fileFilter = $.filter(function (file)
    {
        return file.stat.isFile();
    });

    return gulp.src([
            path.join(conf.paths.assets, '/**/*'),
            path.join('!' + conf.paths.assets, '/**/*.{eot,svg,ttf,woff,woff2}')
        ])
        .pipe(fileFilter)
        .pipe(gulp.dest(path.join(conf.paths.dist, '/')));
});

gulp.task('clean', function ()
{
    return $.del([path.join(conf.paths.dist, '/'), path.join(conf.paths.tmp, '/')]);
});


gulp.task('build', ['css-concat']);
gulp.task('build-min', ['cssmin','js']);