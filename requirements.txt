MySQL-python==1.2.5
Unidecode==0.04.16
billiard==3.3.0.18
Pillow==3.0.0
Django==1.9.0
django-appconf==0.6
django-apptemplates==0.0.1
django-bulk-update==1.1.0
django-cities-light==3.1.2
django-classy-tags==0.5.1
django-imagekit==3.2.7
django-cors-headers==0.12.0
django-cacheops==2.4.1
django-model-utils==2.3.1
django-adminactions==0.8.5

django-material==0.7.0
pysolar==0.6.0
